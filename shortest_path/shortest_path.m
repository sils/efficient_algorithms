function [] = shortest_path(g)
  queue = CQueue();
  queue.push(1);
  num_nodes = size(g, 1)
  
  distances = zeros(num_nodes, 1);
  # Leave dist to the first node zero as it's the starting node
  for i = 2:num_nodes
    distances(i) = Inf
  endfor
  
  parents = zeros(num_nodes, 1);
  
  while ~queue.isempty()
    nodeindex = queue.pop()
    for i = 1:num_nodes
      if i == nodeindex || g(i, nodeindex) == Inf
        continue
      endif
      
      this_costs = g(i, nodeindex) + distances(nodeindex);
      if this_costs < distances(i)
        distances(i) = this_costs;
        parents(i) = nodeindex
        queue.push(i);
      endif
    endfor
    
  endwhile
endfunction