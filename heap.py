from itertools import chain

class Node:
    def __init__(self, value, parent=None, children=[], comp=min, d=2):
        """

        :param value:
        :param parent:
        :param children:
        :param comp:
        :param d: Number of children allowed. (2 = binary heap)
        """
        self.value = value
        self.children = children
        self.parent = parent
        self.comp = comp
        self.d = d

    @property
    def children_count(self):
        return sum(child.children_count for child in self.children)

    @property
    def depth(self):
        return self.parent.depth + 1 if self.parent is not None else 0

    @property
    def height(self):
        return max(chain([child.height+1 for child in self.children], [0]))

    @property
    def rank(self):
        return (1 if len(self.children) < self.d
                else min(child.rank for child in self.children) + 1)

    def make_rank_leftist(self):
        mypos = self.parent.children.index(self)
        for i, child in enumerate(self.parent.children):
            if child is self:
                break

            if child.rank < self.rank:
                self.parent.children[i] = self
                self.parent.children[mypos] = child
                child.make_rank_leftist()
                break


class Heap:
    def __init__(self, root: Node):
        self.root = root

    def insert(self, elem):
        raise NotImplementedError
