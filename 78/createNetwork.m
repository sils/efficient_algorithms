function [network] = createNetwork(n)
%CREATENETWORK Creates a network with n nodes
%   creates directed graph with at most one edge connecting each pair of
%   nodes
%
%   n number of nodes in network
%
%   network connected graph representing network

% The MIT License (MIT)
% Copyright (c) 2016 Timo Neuhäußer
% 
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
% OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
% ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
% OTHER DEALINGS IN THE SOFTWARE.

network = floor(sprand(n, n, 1) * 21);
network(1:n+1:end) = 0;

for r = 1:n
    for c = 1:n
        if network(r,c) > 0 && network(c,r) > 0
            if round(rand())
                network(r,c) = 0;
            else
                network(c,r) = 0;
            end
        end
    end
end
for row = find(all(network==0, 2))
    index = floor(rand() * (n - 1)) + 1;
    if index >= row
        index = index + 1;
    end
    network(row, index) = floor(rand() * 20) + 1;
end
for col = find(all(network==0, 1))
    index = floor(rand() * (n - 1)) + 1;
    if index >= col
        index = index + 1;
    end
    network(index, col) = floor(rand() * 20) + 1;
end

end
