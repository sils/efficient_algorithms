function [flow] = FordFulkerson(network, source, drain)
# Runs the Ford-Fulkerson algorithm over the given network.
#
# @param network: square matrix specifying the flow capacity from each node to
#                 each other node. Values must be positive and only in one
#                 direction.
# @param source:  index of the start node (first one is 1)
# @param drain:   index of end node (first one is 1)
# @return:        a network specifying the actual possible flows.
  flow = zeros(size(network));
  residual = network(:,:);
  if source == drain
    return
  endif
  
  # dijkstra runs in O(|V|^2)
  [e, path] = dijkstra(residual, source, drain);
  while abs(e) < inf
    minval = Inf;
    for i = 1:(length(path)-1)
      if residual(path(i+1), path(i)) > 0
        minval = min(minval, residual(path(i+1), path(i)));
      endif
    endfor
    
    for i = 1:(length(path) - 1)
      residual(path(i+1), path(i)) = residual(path(i+1), path(i)) + minval;
      residual(path(i), path(i+1)) = residual(path(i), path(i+1)) - minval;
      
      if network(path(i), path(i+1)) > 0
        flow(path(i+1), path(i)) = flow(path(i+1), path(i)) + minval;
      else
        flow(path(i), path(i+1)) = flow(path(i), path(i+1)) - minval;
      end
    endfor
    
    [e, path] = dijkstra(residual, source, drain);
    path
    residual
    fflush(stdout);
  endwhile
endfunction