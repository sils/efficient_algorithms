Please note that some files were not authored by me but are pulled in with
explicit permission of the authors under the MIT license. Those sources have an
explicit note in there.

The Ford-Fulkerson algorithm is in FordFulkerson.m.