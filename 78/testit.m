MIN_N = 5
MAX_N = 60
STEP = 5

number = (MAX_N - MIN_N) / STEP;
times = zeros(number, 1);

for N = MIN_N:STEP:MAX_N
  network = createNetwork(N);
  
  tic;
  FordFulkerson(network, 1, 2);
  times(N/STEP) = toc
endfor

times