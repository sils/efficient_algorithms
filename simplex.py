#!/usr/bin/env python3

from numpy import matrix

A = matrix('1,   0.2, 1, 0;'
           '150, 25,  0, 1')
c = [-250, -45, 0, 0]
