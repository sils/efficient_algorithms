function [graph] = createGraph(nodes)
%CREATEGRAPH Creates undirected graph without negative cycles
%
%   nodes Number of nodes for graph
%
%   graph adjacency matrix of graph

% The MIT License (MIT)
% Copyright (c) 2016 Timo Neuhäußer
% 
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
% OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
% ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
% OTHER DEALINGS IN THE SOFTWARE.

success = false;
nodesHalf = floor(nodes / 2);
while ~success
    graph = floor(sprand(nodes, nodes, 0.75) * 41);
    noneZeroes = find(graph ~= 0);
    graph(noneZeroes) = graph(noneZeroes) + 50;
    rows = floor(rand(1, nodesHalf) * nodes) + 1;
    cols = floor(rand(1, nodesHalf) * nodes) + 1;
    values = floor(rand(1, nodesHalf) * 11) - 10;
    graph(sub2ind(size(graph), rows, cols)) = values;
    graph(1:nodes+1:end) = 0;
    try
        graphallshortestpaths(graph);
        success = true;
    catch 
        success = false;
    end
end

end
