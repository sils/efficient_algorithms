function sum = solve_simplex(teachers, lectures, performances)
  len = length(teachers);

  # See http://de.mathworks.com/help/optim/ug/linprog.html#buus4rk-1_1
  #options = optimoptions('linprog','Algorithm','dual-simplex', 'Display', 'off');
  # Reshape matrix into vector
  f = reshape(performances, 1, len*len);
  
  # Set side conditions: sum of all columns and lines must be 1! Note that the
  # matrix got reshaped to a vector. 2 conditions p
  Aeq = zeros(2*len, len*len);
  for variable = 1:len
    # First condition
    Aeq(variable, 1+(variable-1)*len:variable*len) = 1;
    # Second condition
    Aeq(len+variable, variable:len:len*len) = 1;
  end
  # ...must be 1
  beq = ones(1, 2 * len)';
  [x, sum] = linprog(-f',
                     [],
                     [],
                     Aeq,
                     beq,
                     zeros(1, len*len)',
                     []);
  sum = -sum;
end