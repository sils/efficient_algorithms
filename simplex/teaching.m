K_MIN = 5;
K_MAX = 9;
K_RANGE = K_MIN:K_MAX;

simplex_times = zeros(K_MAX-K_MIN,1);
permutation_times = zeros(K_MAX-K_MIN,1);

for k=K_RANGE
  teachers = 1:k;
  lectures = 1:k;
  performances = rand(k, k);
  
  tic;
  sol1 = solve_simplex(teachers, lectures, performances);
  simplex_times(k-K_MIN + 1) = toc;
  
  tic;
  sol2 = solve_permutation(teachers, lectures, performances);
  permutation_times(k-K_MIN + 1) = toc;
  
  if (abs(sol1 - sol2) > 0.0001)
    disp('An error occurred! The results do not match!')
    disp('Simplex:')
    sol1
    disp('Permutation:')
    sol2
  end
end

figure;
title('Runtime comparison: simplex vs. brutal permutations');
xlabel('Teacher/Lecture count');
ylabel('Runtime (Logarithmic)');
hold on;
grid on;
plot(K_RANGE, log(simplex_times))
plot(K_RANGE, log(permutation_times), 'Color', [0.5, 1.0, 0.0])
legend('Simplex', 'Permutation');
hold off;