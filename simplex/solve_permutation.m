function res = solve_permutation(teachers, lectures, performances)
  res = 0;
  len = length(teachers);
  # Vector describes line/column where the 1 in the perm. matrix is
  pms = perms(1:len);
  grid = meshgrid(1:len, 1:factorial(len));
  idx = len*pms - len + grid;
  perm = performances(idx);
  
  [lines, cols] = size(perm);
  for line = 1:lines
    res = max(res, sum(perm(line,:)));
  end
end
