function[G] = generate_graph(n, density);
  # Generates a random matrix representing a connected graph with n elements.
  # Note that the graph is not guaranteed to have n nodes.
  #
  # @param n:       Number of vertices.
  # @param density: Density of the randomly generated matrix.
  G = sparse(n, n);
  current_node = randi([1, n]);
  edges = density*n*(n-1)/2;
  stack = zeros(edges, 1);

  for stack_id = 1:edges
    # If the current node doesn't have any possible edges to draw anymore, go
    # back on the stack to find one. One zero on the diagonal is always there.
    j = 1;
    while n-nnz(G(:,current_node)) < 2
      current_node = stack(stack_id-j);
      j = j+1;
    endwhile

    # Get a random node that is not connected to current yet
    stack(stack_id) = current_node;
    connect_node = randi([1, n]);
    while connect_node == current_node || G(connect_node, current_node) ~= 0
      connect_node = randi([1, n]);
    endwhile

    # Connect them!
    G(connect_node, current_node) = G(current_node, connect_node) = rand(1, 1);

    current_node = connect_node;
  endfor
endfunction
