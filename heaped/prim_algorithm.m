function [G_min] = prim_algorithm(G)
  # Applies the prim algorithm onto the strongly connected graph G to find a
  # minimal spanning tree which includes every vertex of the graph.
  #
  # The graph is expected to have at least one vertex, otherwise bad things
  # *will* happen! However, the graph may not include all vertices (then those
  # not included will of course not be in the spanning tree :))
  #
  # @param G: The adjacency matrix of the graph to apply prim to.
  [n, n] = size(G);
  G_min = sparse(n, n);
  # Holds the difference between G and G_min, i.e. addable weightings.
  G_diff = G;
  max_weight = full(max(max(G)));

  # List of known nodes, assign 1 to a node to mark it known
  known_nodes = zeros(n, 1);

  # Get a random start node that is connected to another node.
  current_node = randi([1, n]);
  while nnz(G(:, current_node)) == 0
    current_node = randi([1, n]);
  endwhile
  known_nodes(current_node) = 1;

  while 1
    current_node = 0;
    min_weight = max_weight;

    for node = 1:n
      # We know this node and it may provide a better weight than the last one
      if known_nodes(node) ~= 0 && min(nonzeros(G_diff(:, node))) <= min_weight
        for connect_node = 1:n
          if known_nodes(connect_node) == 0 && G_diff(node, connect_node) ~= 0 && G_diff(node, connect_node) <= min_weight
            new_node = connect_node;
            current_node = node;
            min_weight = G_diff(node, connect_node);
          endif
        endfor
      endif
    endfor

    # We didn't find any node to connect to and we're done!
    if current_node == 0
      return;
    endif

    G_min(current_node, new_node) = G_min(new_node, current_node) = min_weight;
    G_diff(current_node, new_node) = G_diff(new_node, current_node) = 0;
    known_nodes(new_node) = 1;
  endwhile
endfunction
