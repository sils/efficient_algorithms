Task 4
======

> **Hint**: This document is best viewed after rendered by a Markdown parser.
> `pandoc README.md -o README.pdf` will do the trick if you have pandoc
> installed. Otherwise plain text will also do of course since Markdown is
> optimized for great readability. :)

> **Warning**: The code has been written for Octave. It will be runnable in Matlab
> but may need slight adaptions.

> **Yet another hint**: This document, and all related files, are distributed
> under the GNU AFFERO GENERAL PUBLIC LICENSE. See `LICENSE` for more
> information about your rights with this software.

Task 4.1
--------

The implementation for the minimum spanning tree via a simple Prim algorithm
(using the adjacency matrix) is contained in `prim_algorithm.m`. You can test
it with:

```octave
A = generate_graph(500, 0.1);
A_min = prim_algorithm(A);
```

> Note: You can verify that the density of the matrix is correct. Note that the
> adjacency matrix will be generated such that it has the given density,
> *excluding the diagonal elements* which are always zero:
> ```
> nnz(A)/(prod(size(A))-size(A, 1))  # 0.1
> ```
>
> Because the `generate_graph` function is just a utility function to generate
> test data, it is written in a rather simple and inefficient way (especially
> when the density is high).

You can easily see how the number of edges in the adjacency matrix is reduced
by the prim algorithm with:

```octave
spy(A);
```

![Density of the Full Graph](500_0.1_full_graph.png)
\

```
figure;
spy(A_min);
```

![Density of the Minimal Spanning Tree](500_0.1_min_graph.png)
\

```
nnz(A)
# 24950 (number might vary due to random graph generation)
nnz(A_min)
# 998 (number might vary due to random graph generation)
```

The performance and complexity of the algorithm can be shown experimentally
with:

```octave
perf_primitive_prim
# A graph with up to 20 nodes takes 0.013739 seconds.
# A graph with up to 40 nodes takes 0.099081 seconds.
# A graph with up to 60 nodes takes 0.214658 seconds.
# A graph with up to 80 nodes takes 0.559960 seconds.
# A graph with up to 100 nodes takes 1.097647 seconds.
# A graph with up to 120 nodes takes 1.529207 seconds.
# A graph with up to 140 nodes takes 2.388772 seconds.
# A graph with up to 160 nodes takes 3.982129 seconds.
# A graph with up to 180 nodes takes 5.365328 seconds.
# A graph with up to 200 nodes takes 6.361563 seconds.
# A graph with up to 220 nodes takes 9.746168 seconds.
# A graph with up to 240 nodes takes 9.134254 seconds.
# A graph with up to 260 nodes takes 16.617807 seconds.
# A graph with up to 280 nodes takes 16.604443 seconds.
# A graph with up to 300 nodes takes 21.612393 seconds.
```

![Plot of the Values Given Above](20-300_performance.png)
\


The graphic shows straightforward that the complexity is polynomial.

Task 4.2
--------

> Note: that this task does not use the same algorithm as task 4.1. This new
> algorithm uses a heap priority queue. You are hereby made aware that the Heap
> implementation is not done by the author of this document but was distributed
> to him under the MIT license, redistributed with this package.

The plot below shows the heap arity for adjacency matrices with certain
densities.

```octave
eval_heap
```

![Time vs. Arity Plot](density_time_arity.png)
\


There seems to be the general trend that a higher arity results in a lower
computation time. However it is not possible to determine an optimal arity (even
dependent on n/m) from this data.

(Please note that the density of the symmetric adjacency matrix is calculatable
by `nnz(A)/(prod(size(A))-size(A, 1))`.)
