function [G_min, num_nodes] = heaped_prim(G, d)
  # Applies the prim algorithm onto the strongly connected graph G to find a
  # minimal spanning tree which includes every vertex of the graph.
  #
  # This algorithm is performance efficient - it uses a heap.
  #
  # The graph is expected to have at least one vertex, otherwise bad things
  # *will* happen! However, the graph may not include all vertices (then those
  # not included will of course not be in the spanning tree :))
  #
  # The graph is expected to be strongly connected, otherwise, again, bad things
  # will happen.
  #
  # @param G: The adjacency matrix of the graph to apply prim to.
  [n, n] = size(G);
  G_min = sparse(n, n);
  max_weight = full(max(max(G)));

  # Get the actual number of nodes that are connected to the graph
  num_nodes = 0;
  for i = 1:n
    if nnz(G(:, i)) > 0
      num_nodes = num_nodes + 1;
    endif
  endfor

  # Initialize Heap
  nodelist = zeros(num_nodes, 2);
  curr = 1;
  for i=1:n
    if nnz(G(:, i)) > 0
      nodelist(curr, 1) = i;
      nodelist(curr, 2) = Inf;
      curr = curr + 1;
    endif
  endfor

  # Start at first node
  nodelist(1, 2) = 0;
  heap = dHeap(nodelist, d);

  while ~heap.isEmpty()
    [heap, node, key] = heap.extractMin();
    for i = 1:n
      if i ~= node && G(i, node) ~= 0 && G(i, node) < key
        G_min(node, i) = G_min(i, node) = key = G(i, node);
        heap.decreaseKey(i, key);
      endif
    endfor
  endwhile

endfunction
