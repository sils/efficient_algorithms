NUM_TESTS = 15;
MULTIPLIER = 20;

times = zeros(NUM_TESTS, 1);

for i = 1:NUM_TESTS
  A = generate_graph(i*MULTIPLIER, 0.1);
  tic;
  prim_algorithm(A);
  times(i) = toc;
  fprintf("A graph with up to %d nodes takes %f seconds.\n", i*MULTIPLIER, times(i));
  fflush(stdout);
endfor

plot((1:NUM_TESTS)*MULTIPLIER, times)
grid on
xlabel("Vertices (#)")
ylabel("Time (s)")
