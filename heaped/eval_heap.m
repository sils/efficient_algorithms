D_END = 10;
MAX_NODES = 500;

grid on;
for density = [0.1, 0.5, 1]
  A = generate_graph(MAX_NODES, density);
  times = zeros(D_END-1, 1);
  for d = 1:D_END
    tic;
    [G_min, num_nodes] = heaped_prim(A, d*2);
    times(d) = toc;
    d
    fflush(stdout);
  endfor
  p = plot((1:D_END)*2, times, 'Color', [density, 0.5, density]);
  hold on;
  legend([p], sprintf("Density: %f; Nodes: %d; Edges: %d", density, num_nodes, nnz(A)/2));
  xlabel("Heap Arity");
  ylabel("Needed time (s)");
endfor
hold off;
