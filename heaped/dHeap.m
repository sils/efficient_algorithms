% The MIT License (MIT)
% Copyright (c) 2016 Timo Neuhäußer
%
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
%
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
%
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
% OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
% ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
% OTHER DEALINGS IN THE SOFTWARE.

classdef dHeap
    %DHEAP simple implementation of d-ary heap
    %   d-ary heap data structure

    properties (Access = private)
        nodesAndKeys % node and key pair in heap order
        %indices % index of node, indexed by node
        size % number of elements currently present in heap
        arity
    end

    methods (Access = public)
        function heap = dHeap(nodesAndKeys, arity)
            %dHeap Create d-ary heap
            %   nodesAndKeys n*2 matrix, first column id of node, second
            %       column value of node e.g. [1, 4; 2, 5; 3, 1]
            %   arity artity of heap, maximum number of children of node
            if nargin > 0
                numElements = size(nodesAndKeys, 1);
                heap.size = 0;
                heap.nodesAndKeys = ones(numElements, 2).*inf;
                %heap.indices = zeros(numElements, 1);
                heap.arity = arity;
                for i = 1:numElements
                    heap = heap.insert(nodesAndKeys(i, 1), nodesAndKeys(i, 2));
                end
            else
                error('too few arguments, nodes and keys required');
            end
        end
        function isEmpty = isEmpty(heap)
            isEmpty = heap.size < 1;
        end
        function [heap, node, key] = extractMin(heap)
            if heap.size > 0
                node = heap.nodesAndKeys(1, 1);
                key = heap.nodesAndKeys(1, 2);
                heap = heap.swap(1, heap.size);
                heap.nodesAndKeys(heap.size, :) = [inf, inf];
                heap.size = heap.size - 1;
                heap = heap.siftDown(1);
            else
                node = inf;
                key = inf;
            end
        end
        function heap = decreaseKey(heap, node, newValue)
            % nicht in log_d(n) aber mit find schneller als mit extra indexing
            % bei per node
            idx = find(heap.nodesAndKeys(:, 1) == node, 1);
            %idx = heap.indices(node);
            heap.nodesAndKeys(idx, 2) = newValue;
            heap = heap.siftUp(idx);
        end
        function print(heap)
            disp(heap.nodesAndKeys);
        end
    end
    methods (Access = private)
        function heap = insert(heap, node, key)
            heap.size = heap.size + 1;
            heap.nodesAndKeys(heap.size, :) = [node, key];
            %heap.indices(node) = heap.size;
            heap = heap.siftUp(heap.size);
        end
        function heap = siftUp(heap, index)
            if index > 1
               parentIndex = heap.getParent(index);
               if heap.nodesAndKeys(parentIndex, 2) > heap.nodesAndKeys(index, 2)
                   heap = heap.swap(index, parentIndex);
                   heap = heap.siftUp(parentIndex);
               end
            end
        end
        function heap = siftDown(heap, index)
            childIndices = heap.getChildren(index);
            if ~isempty(childIndices)
                [minValue, minIndex] = min(heap.nodesAndKeys(childIndices, 2));
                if minValue < heap.nodesAndKeys(index, 2)
                    newPos = childIndices(minIndex);
                    heap = heap.swap(index, newPos);
                    heap = heap.siftDown(newPos);
                end
            end
        end
        function heap = swap(heap, index1, index2)
            heap.nodesAndKeys([index1, index2], :) = heap.nodesAndKeys([index2, index1], :);
            %node1 = heap.nodesAndKeys(index1, 1);
            %node2 = heap.nodesAndKeys(index2, 1);
            %heap.indices([node1, node2], 1) = heap.indices([node2, node1], 1);
        end
        function childIndices = getChildren(heap, index)
            firstChildIndex = (index - 1) * heap.arity + 2;
            if firstChildIndex > heap.size
                childIndices = [];
            else
                childIndices = firstChildIndex:min(firstChildIndex + heap.arity - 1, heap.size);
            end
        end
        function parentIndex = getParent(heap, index)
            if index < 2
                parentIndex = 0;
            else
                parentIndex = floor((index - 2) / heap.arity) + 1;
            end
        end
    end

end

